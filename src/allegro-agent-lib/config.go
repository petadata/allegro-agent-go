package allegro_agent_lib

import "gopkg.in/resty.v0"

func GetClient() *resty.Client {
	return resty.New()
}

func GetRequest() *resty.Request {
	c := GetClient()
	return c.R()
}

type config struct {
	BaseUrl string
}

var sharedInstance *config = newConfig()

func newConfig() *config {
	return &config{BaseUrl:"https://allegrosmart_host_name/api"}
}

func GetConfig() *config {
	return sharedInstance
}
