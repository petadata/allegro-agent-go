package allegro_agent_lib


import (
	"gopkg.in/resty.v0"
)

func GetMetrics(apikey string) (*resty.Response, error) {
	c := GetClient()
	c.SetQueryParam("apikey", apikey)

	// GET request
	baseUrl := GetConfig().BaseUrl
	resp, err := c.R().Get(baseUrl + "/v3.7/metrics")
	return resp, err
}
