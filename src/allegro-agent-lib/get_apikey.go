package allegro_agent_lib


import (
	"gopkg.in/resty.v0"
	"strconv"
)

func GetApikey(username, password string, userProfile bool) (*resty.Response, error) {
	c := GetClient()
	c.SetQueryParam("username", username).
	SetQueryParam("password", password).
	SetQueryParam("userProfile", strconv.FormatBool(userProfile))

	// GET request
	baseUrl := GetConfig().BaseUrl
	resp, err := c.R().Get(baseUrl + "/v3.7/apikey1/.json")
	return resp, err
}
