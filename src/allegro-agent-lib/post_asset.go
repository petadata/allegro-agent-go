package allegro_agent_lib


import (
	"gopkg.in/resty.v0"
	"fmt"
	//"encoding/json"
)

type Asset struct {
	Apikey string		`json:"apikey"`
	RequestId string	`json:"requestId"`
	Name string		`json:"name"`
	ModelNo string		`json:"modelNo"`
	ProductionNo string	`json:"productionNo"`
}

func CreateAsset(connectionNo string, asset Asset) (*resty.Response, error) {
	c := GetClient()

	// debug
	//body, _ := json.Marshal(asset)
	//fmt.Printf("%s\n", string(body))

	// POST request
	baseUrl := GetConfig().BaseUrl
	url := fmt.Sprintf(baseUrl + "/v3.7/connections/%s/assets", connectionNo)
	//fmt.Println("[Target] " + url)
	// assetはrestyが自動的にJSONにエンコードする
	resp, err := c.R().SetBody(asset).Post(url)

	return resp, err
}
