package allegro_agent_lib


import (
	"gopkg.in/resty.v0"
	"strconv"
	"fmt"
)

func GetAssets(userNo, apikey string, withSharedAsset, withLocation bool) (*resty.Response, error) {
	c := GetClient()
	c.SetQueryParam("apikey", apikey).
	SetQueryParam("withSharedAsset", strconv.FormatBool(withSharedAsset)).
	SetQueryParam("withLocation", strconv.FormatBool(withLocation))

	// GET request
	baseUrl := GetConfig().BaseUrl
	url := fmt.Sprintf(baseUrl + "/v3.7/user/%s/assets", userNo)
	resp, err := c.R().Get(url)
	return resp, err
}
