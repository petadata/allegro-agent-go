package allegro_agent_lib


import (
	"gopkg.in/resty.v0"
	"fmt"
	//"encoding/json"
)

func RegisterData(connectionNo, assetNo, format, apikey, dataType, payload, contentType string) (*resty.Response, error) {
	c := GetClient()
	c.SetQueryParam("apikey", apikey).
	SetQueryParam("dataType", dataType)

	// POST request
	baseUrl := GetConfig().BaseUrl
	url := fmt.Sprintf(baseUrl + "/v3.7/connections/%s/assets/%s/datastreams/%s", connectionNo, assetNo, format)
	fmt.Println("[Target] " + url)

	resp, err := c.R().SetHeader("Content-Type", contentType).SetBody(payload).Post(url)

	return resp, err
}
