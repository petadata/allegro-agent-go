package main


import (
	"fmt"
	allegro "../allegro-agent-lib"
	"gopkg.in/resty.v0"
	"strconv"
)

func showResponse(resp *resty.Response, err error) (*resty.Response, error) {
	// explore response object
	if err == nil {
		fmt.Printf("Response Status Code: %v\n", resp.StatusCode())
		//fmt.Printf("\nResponse Status: %v", resp.Status())
		//fmt.Printf("\nResponse Time: %v", resp.Time())
		//fmt.Printf("\nResponse Recevied At: %v", resp.ReceivedAt())
		//fmt.Printf("\nResponse Body: %v", string(resp.Body()))
		fmt.Printf(string(resp.Body()))
		fmt.Println()
	} else {
		fmt.Printf("Error: %v", err)
		fmt.Println()
	}

	return  resp, err
}

func setBaseUrl(baseUrl string) {
	allegro.GetConfig().BaseUrl = baseUrl
}

func getApikey(username, password, userProfile string) (*resty.Response, error) {
	userProfileBool, convErr := strconv.ParseBool(userProfile)
	if convErr != nil {
		return nil, convErr
	}
	resp, err := allegro.GetApikey(username, password, userProfileBool)
	return resp, err
}

func getAssets(userNo, apikey, withSharedAsset, withLocation string) (*resty.Response, error) {
	withSharedAssetBool, convErr1 := strconv.ParseBool(withSharedAsset)
	if convErr1 != nil {
		return nil, convErr1
	}
	withLocationBool, convErr2 := strconv.ParseBool(withLocation)
	if convErr1 != nil {
		return nil, convErr2
	}
	resp, err := allegro.GetAssets(userNo, apikey, withSharedAssetBool, withLocationBool)
	return resp, err
}

func getMetrics(apikey string) (*resty.Response, error) {
	resp, err := allegro.GetMetrics(apikey)
	return resp, err
}

func createAsset(connectionNo, apikey, requestId, name, modelNo, productionNo string) (*resty.Response, error) {
	asset := allegro.Asset{ apikey, requestId, name, modelNo, productionNo }
	resp, err := allegro.CreateAsset(connectionNo, asset)
	return resp, err
}

func registerData(connectionNo, assetNo, format, apikey, dataType, payload, contentType string) (*resty.Response, error) {
	resp, err := allegro.RegisterData(connectionNo, assetNo, format, apikey, dataType, payload, contentType)
	return resp, err
}

func main() {
	//setBaseUrl("http://10.0.0.26:9000/api")
	setBaseUrl("https://idea.allegrosmart.com/api")

	// Apikey
	//resp, err := getApikey("sue@allegrosmart.com", "suesue", "false")

	// Assets
	//resp, err := getAssets("u3500516", "46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417", "false", "false")

	// Metrics
	//resp, err := getMetrics("46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417")

	// Create Asset
	/*
	resp, err := createAsset("new",
		"46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417",
		"req-0004",
		"testName",
		"testModelNo",
		"testProductionNo")
	*/

	// Register Asset
	resp, err := registerData("1",
		"1",
		".json",
		"46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417",
		"application/json",
		`{"date":"2016-09-14 00:00:00.000", "value":10.0}`,
		"application/json")

	// explore response object
	showResponse(resp, err)
}