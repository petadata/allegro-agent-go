package main


import (
	"fmt"
	allegro "../allegro-agent-lib"
	"C"
)

//export get_assets
func get_assets() {
	// Apikey
	//resp, err := allegro.GetApikey("sue@allegrosmart.com", "suesue", false)

	// Assets
	resp, err := allegro.GetAssets("u3500516", "46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417", false, false)

	// Metrics
	//resp, err := allegro.GetMetrics("46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417")

	// Create Asset
	/*
	asset := allegro.Asset{
		"46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163417",
		"req-0003",
		"testName",
		"testModelNo",
		"testProductionNo",
	}
	resp, err := allegro.CreateAsset("new", asset)
	*/

	// explore response object
	if err == nil {
		fmt.Printf("\nResponse Status Code: %v", resp.StatusCode())
		fmt.Printf("\nResponse Status: %v", resp.Status())
		fmt.Printf("\nResponse Time: %v", resp.Time())
		fmt.Printf("\nResponse Recevied At: %v", resp.ReceivedAt())
		fmt.Printf("\nResponse Body: %v", string(resp.Body()))
		fmt.Println()
	} else {
		fmt.Printf("\nError: %v", err)
		fmt.Println()
	}
}

func main() {
}
