# Goで実装したロジックをShared Library化する

## 参考
http://qiita.com/yanolab/items/1e0dd7fd27f19f697285

## 条件
- パッケージ "C" をインポートする
- エクスポートしたい関数に `//export hoge` というアノテーションを付ける
- 対象ファイルはmainパッケージ所属でないといけない
- `main()` が必要(内容は空でよい)

## ビルド方法
```
$ go get gopkg.in/go-resty/resty.v0
$ go build -buildmode=c-shared -o liballegro.so liballegro.go
```
確認
```
$ ls -l
total 12992
-rw-r--r-- 1 nobusue staff    1116  9 14 16:45 liballegro.go
-rw-r--r-- 1 nobusue staff    1280  9 14 16:45 liballegro.h
-rw-r--r-- 1 nobusue staff 6974148  9 14 16:45 liballegro.so
```
.soと.hができていればOK

## Shared Libraryのテスト
Cで実装するのは面倒なので、Pythonから読み込んでテストする
```
$ python
Python 2.7.12 (default, Jun 29 2016, 14:05:02) 
[GCC 4.2.1 Compatible Apple LLVM 7.3.0 (clang-703.0.31)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import ctypes
>>> lib = ctypes.CDLL("./liballegro.so")
>>> lib.get_assets()

Response Status Code: 200
Response Status: 200 OK
Response Time: 698.92609ms
Response Recevied At: 2016-09-14 16:46:39.811370487 +0900 JST
Response Body: [{"contractorNo":"idea","model":"Canaria_01","_modified":1470204467398,"enable":true,"updatedToday":false,"alertEnable":"false","id":"5772bd52e4b0c08ea4551d5f","username":"sue@allegrosmart.com","userNoText":"u3500516","userId":"55c01eaee4b096ac582d4b74","name":"Canaria_01_201606181030922","connectionNo":2,"mobility":"immovable","assetNo":2,"installLocation":{"address":"","facility":"","latitude":0.0,"longitude":0.0,"name":""},"lastUpdated":1470204467398,"extension":{},"pinCodeExpired":1467137362647,"new":false,"_created":1467137362647,"userNo":"u3500516","pinAvailable":false,"useAlertFunc":"disable","dateCreated":"2016/06/29 03:09:22"},{"contractorNo":"idea","model":"Canaria_01","_modified":1470204467395,"enable":true,"updatedToday":false,"alertEnable":"false","id":"5772b3a1e4b0c08ea4550e43","username":"sue@allegrosmart.com","userNoText":"u3500516","userId":"55c01eaee4b096ac582d4b74","name":"Canaria_01_201606181022801","connectionNo":1,"mobility":"immovable","assetNo":1,"installLocation":{"address":"","facility":"","latitude":0.0,"longitude":0.0,"name":""},"lastUpdated":1470204467395,"extension":{},"pinCodeExpired":1467134881663,"new":false,"_created":1467134881663,"userNo":"u3500516","pinAvailable":false,"useAlertFunc":"disable","dateCreated":"2016/06/29 02:28:01"}]
0
>>> 
```
問題なく実行できた。
あとはパラメータの受け渡しなどが課題。
