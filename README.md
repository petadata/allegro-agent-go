# AllegroSmart library #

This library is a simple, multi-platform SDK for AllegroSmart.

it is for building web, server, edge-device, and mobile apps.

This library wraps the AllegroSmart REST API.

Go-Lang ver > 1.5　(recommended version 1.6)

### Functions
* get_apikey : get APIkey(=security token) - It is necessary to be a pre-registered user
* get_assets : get Assets.
* get_metrics : Get Metrics. （temperature, humidity, ... etc.）.
* post_asset : add new Assets or update Asset attribute.
* post_data  : post Sensing data to AllegroSmart cloud.

#### 
see also. [AllegroSmart API Reference](http://dev.allegrosmart.com/docs/)


# Building Lib #
### Pre-Building Config
```
vi  src/allegro-agent-lib/config.go
```
change 'allegrosmart_host_name' to real hostname (for example. abdc.allegrosmart.com).
``` 
20| func newConfig() *config {
21| 	return &config{BaseUrl:"https://allegrosmart_host_name/api"}
22| }
```
 ↓
``` 
20| func newConfig() *config {
21| 	return &config{BaseUrl:"https://abcd.allegrosmart.com/api"}
22| }
```


### Build
```
$ export GOPATH=$HOME/goroot
$ cd src/main
$ go get gopkg.in/go-resty/resty.v0
$ go get gopkg.in/resty.v0
$ go build -buildmode=c-shared -o liballegro.so liballegro.go
```

### Build linux-386
```
$ export GOPATH=$HOME/goroot
$ cd src/main
$ go get gopkg.in/go-resty/resty.v0
$ go get gopkg.in/resty.v0
$ GOOS=linux GOARCH=386 go build -buildmode=c-shared -o liballegro.so liballegro.go
```

### Build linux-arm
```
$ export GOPATH=$HOME/goroot
$ cd src/main
$ go get gopkg.in/go-resty/resty.v0
$ go get gopkg.in/resty.v0
$ GOOS=linux GOARCH=arm go build -buildmode=c-shared -o liballegro.so liballegro.go
```


### Other
```
 $GOOS   $GOARCH
    darwin  386
    darwin  amd64
    darwin  arm
    darwin  arm64
    dragonfly   amd64
    freebsd 386
    freebsd amd64
    freebsd arm
    linux   386
    linux   amd64
    linux   arm
    linux   arm64
    linux   ppc64
    linux   ppc64le
    linux   mips64
    linux   mips64le
    netbsd  386
    netbsd  amd64
    netbsd  arm
    openbsd 386
    openbsd amd64
    openbsd arm
    plan9   386
    plan9   amd64
    solaris amd64
    windows 386
    windows amd64
```

### Build results
```
$ ls *.h; ls *.so
liballegro.h
liballegro.so

```

### Using
#### go-lang sample
```
package main


import (
	"fmt"
	allegro "../allegro-agent-lib"
	"gopkg.in/resty.v0"
	"strconv"
)

func showResponse(resp *resty.Response, err error) (*resty.Response, error) {
	// explore response object
	if err == nil {
		fmt.Printf("Response Status Code: %v\n", resp.StatusCode())
		//fmt.Printf("\nResponse Status: %v", resp.Status())
		//fmt.Printf("\nResponse Time: %v", resp.Time())
		//fmt.Printf("\nResponse Recevied At: %v", resp.ReceivedAt())
		//fmt.Printf("\nResponse Body: %v", string(resp.Body()))
		fmt.Printf(string(resp.Body()))
		fmt.Println()
	} else {
		fmt.Printf("Error: %v", err)
		fmt.Println()
	}

	return  resp, err
}

func setBaseUrl(baseUrl string) {
	allegro.GetConfig().BaseUrl = baseUrl
}

func getApikey(username, password, userProfile string) (*resty.Response, error) {
	userProfileBool, convErr := strconv.ParseBool(userProfile)
	if convErr != nil {
		return nil, convErr
	}
	resp, err := allegro.GetApikey(username, password, userProfileBool)
	return resp, err
}

func getAssets(userNo, apikey, withSharedAsset, withLocation string) (*resty.Response, error) {
	withSharedAssetBool, convErr1 := strconv.ParseBool(withSharedAsset)
	if convErr1 != nil {
		return nil, convErr1
	}
	withLocationBool, convErr2 := strconv.ParseBool(withLocation)
	if convErr1 != nil {
		return nil, convErr2
	}
	resp, err := allegro.GetAssets(userNo, apikey, withSharedAssetBool, withLocationBool)
	return resp, err
}

func getMetrics(apikey string) (*resty.Response, error) {
	resp, err := allegro.GetMetrics(apikey)
	return resp, err
}

func createAsset(connectionNo, apikey, requestId, name, modelNo, productionNo string) (*resty.Response, error) {
	asset := allegro.Asset{ apikey, requestId, name, modelNo, productionNo }
	resp, err := allegro.CreateAsset(connectionNo, asset)
	return resp, err
}

func registerData(connectionNo, assetNo, format, apikey, dataType, payload, contentType string) (*resty.Response, error) {
	resp, err := allegro.RegisterData(connectionNo, assetNo, format, apikey, dataType, payload, contentType)
	return resp, err
}

func main() {
	setBaseUrl("https://your_allegro_hostname/api")

	// Apikey
	//resp, err := getApikey("xxx@allegrosmart.com", "password", "false")

	// Assets
	//resp, err := getAssets("u3500999", "46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163418", "false", "false")

	// Metrics
	//resp, err := getMetrics("46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163418")

	// Create Asset
	/*
	resp, err := createAsset("new",
		"46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163418",
		"req-0004",
		"testName",
		"testModelNo",
		"testProductionNo")
	*/

	// Register Asset
	resp, err := registerData("1",
		"1",
		".json",
		"46204F50748D63EDF1BF0C3CE233E35794AC78F24221AB2C150023AB50163418",
		"application/json",
		`{"date":"2016-09-14 00:00:00.000", "value":10.0}`,
		"application/json")

	// explore response object
	showResponse(resp, err)
}
```

#### python sample
```
$ python
Python 2.7.12 (default, Jun 29 2016, 14:05:02) 
[GCC 4.2.1 Compatible Apple LLVM 7.3.0 (clang-703.0.31)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import ctypes
>>> lib = ctypes.CDLL("./liballegro.so")
>>> lib.get_assets()

Response Status Code: 200
Response Status: 200 OK
Response Time: 698.92609ms
Response Recevied At: 2016-09-14 16:46:39.811370487 +0900 JST
Response Body: [{"contractorNo":"idea","model":"Canaria_01","_modified":1470204467398,"enable":true,"updatedToday":false,"alertEnable":"false","id":"5772bd52e4b0c08ea4551d5f","username":"sue@allegrosmart.com","userNoText":"u3500516","userId":"55c01eaee4b096ac582d4b74","name":"Canaria_01_201606181030922","connectionNo":2,"mobility":"immovable","assetNo":2,"installLocation":{"address":"","facility":"","latitude":0.0,"longitude":0.0,"name":""},"lastUpdated":1470204467398,"extension":{},"pinCodeExpired":1467137362647,"new":false,"_created":1467137362647,"userNo":"u3500516","pinAvailable":false,"useAlertFunc":"disable","dateCreated":"2016/06/29 03:09:22"},{"contractorNo":"idea","model":"Canaria_01","_modified":1470204467395,"enable":true,"updatedToday":false,"alertEnable":"false","id":"5772b3a1e4b0c08ea4550e43","username":"sue@allegrosmart.com","userNoText":"u3500516","userId":"55c01eaee4b096ac582d4b74","name":"Canaria_01_201606181022801","connectionNo":1,"mobility":"immovable","assetNo":1,"installLocation":{"address":"","facility":"","latitude":0.0,"longitude":0.0,"name":""},"lastUpdated":1470204467395,"extension":{},"pinCodeExpired":1467134881663,"new":false,"_created":1467134881663,"userNo":"u3500516","pinAvailable":false,"useAlertFunc":"disable","dateCreated":"2016/06/29 02:28:01"}]
0
>>> 
```